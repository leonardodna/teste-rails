require 'rails_helper'

RSpec.describe Color, type: :model do
  it { is_expected.to have_many(:units) }

  it { is_expected.to validate_presence_of(:name) }
end
