require 'rails_helper'

RSpec.describe Unit, type: :model do
  it { is_expected.to belong_to(:product) }
  it { is_expected.to belong_to(:color) }
  it { is_expected.to belong_to(:size) }

  it { is_expected.to validate_numericality_of(:quantity).only_integer.is_greater_than_or_equal_to(0) }
end
