Color.create([
  { name: 'Vermelho' },
  { name: 'Laranja' },
  { name: 'Amarelo' },
  { name: 'Verde' },
  { name: 'Azul' },
  { name: 'Violeta' }
])

Size.create([
  { name: 'P' },
  { name: 'M' },
  { name: 'G' },
  { name: 'GG' },
  { name: '3G' },
  { name: '4G' }
])
