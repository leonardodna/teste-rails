class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.references :product, index: true, foreign_key: true
      t.references :color, index: true, foreign_key: true
      t.references :size, index: true, foreign_key: true
      t.integer :quantity, null: false, default: 0

      t.timestamps null: false
    end
  end
end
