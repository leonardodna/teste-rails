$().ready(function(){
  $('.table-units').on('click', '.unit-delete', event => {
    event.preventDefault()
    event.target.closest('tr').remove()
  })

  $('.table-units').on('click', '.unit-add', event => {
    event.preventDefault()
    tr = $(".table-units tbody tr:first").clone()
    tr.find('select, input').each((_, element) => { $(element).val('') });
    tr.appendTo(".table-units tbody");
  })
})