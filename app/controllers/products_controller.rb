class ProductsController < ApplicationController

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
    @product.units.build
  end

  def create
    @product = Product.new(product_params)

    if @product.save!
      redirect_to products_path, notice: 'Produto cadastrado com sucesso!'
    else
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])

    if @product.update(product_params)
      redirect_to products_path, notice: 'Produto atualizado com sucesso!'
    else
      render :new
    end
  end

  def destroy
    Product.find(params[:id]).destroy

    redirect_to products_path, notice: 'Unidade removida com sucesso'
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, units_attributes: [:id, :color_id, :size_id, :quantity])
  end

end
