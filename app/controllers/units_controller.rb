class UnitsController < ApplicationController
  def destroy
    Unit.find(params[:id]).destroy!

    redirect_to products_path, notice: 'Unidade removida com sucesso'
  end
end
