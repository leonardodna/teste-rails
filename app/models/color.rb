class Color < ActiveRecord::Base
  has_many :units

  validates :name, presence: true

  def to_s
    name
  end
end
