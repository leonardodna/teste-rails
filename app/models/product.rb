class Product < ActiveRecord::Base
  has_many :units, dependent: :destroy

  accepts_nested_attributes_for :units

  validates :name, presence: true
end
