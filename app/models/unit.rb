class Unit < ActiveRecord::Base
  belongs_to :product
  belongs_to :color
  belongs_to :size

  validates :color, :size, presence: true

  validates :quantity, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }
end
