FactoryBot.define do
  factory :product do
    sequence(:name) { |n| "Product ##{n}" }
    description { "MyText" }
  end
end
