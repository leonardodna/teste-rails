FactoryBot.define do
  factory :color do
    sequence (:name) { |n| "Color ##{n}" }
  end
end
