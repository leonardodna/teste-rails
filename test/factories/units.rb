FactoryBot.define do
  factory :unit do
    product { nil }
    color { nil }
    size { nil }
    quantity { 1 }
  end
end
