Rails.application.routes.draw do
  resources :products, except: :show

  resources :units, only: :destroy

  root "products#index"
end
